# protected-environments-via-api


1. Create a new project, with CI that creates an environments

```yaml
stages:
- test
- deploy

test:
  stage: test
  script:
  - 'echo "Testing Application: ${CI_PROJECT_NAME}"'

production:
  stage: deploy
  when: manual
  script:
  - 'echo "Deploying to ${CI_ENVIRONMENT_NAME}"'
  environment:
    name: ${CI_JOB_NAME}
```

2. Via UI, create a new group

- `protected-access-group`
- group id: 9899826

![protected-access-group](./protected-access-group.png)

3. Via API, add a user to the group as a reporter

```sh
curl --request POST --header "PRIVATE-TOKEN: xxxxxxxxxxxx" --data "user_id=3222377&access_level=20" "https://gitlab.com/api/v4/groups/9899826/members"

{"id":3222377,"name":"Sean Carroll","username":"sfcarroll","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/3222377/avatar.png","web_url":"https://gitlab.com/sfcarroll","access_level":20,"created_at":"2020-10-26T17:37:50.309Z","expires_at":null}
```

4. Via API, add the group to the project, as a reporter 

```sh
curl --request POST --header "PRIVATE-TOKEN: xxxxxxxxxxxx" --request POST "https://gitlab.com/api/v4/projects/22034114/share?group_id=9899826&group_access=20"

{"id":1233335,"project_id":22034114,"group_id":9899826,"group_access":20,"expires_at":null}
```


5. Via API, add the group with protected environment access

```sh
curl -v -k --header 'Content-Type: application/json' --request POST --data '{"name": "production", "deploy_access_levels": [{"group_id": 9899826}]}' --header "PRIVATE-TOKEN: xxxxxxxxxxx" "https://gitlab.com/api/v4/projects/22034114/protected_environments"
```

6. Group now has access and can be seen in the UI

![protected-access-group](./protected-environments.png)

References 

- https://gitlab.com/gitlab-org/gitlab/-/issues/30595
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38188

